# Caddy docker

A docker image of Caddy based on the [abiosoft/caddy-docker](https://github.com/abiosoft/caddy-docker) `php-no-stats` image.

Includes the Caddy plugins `prometheus,reauth,git,cache`, more php extensions and drops privilages to UID/GID 1005.