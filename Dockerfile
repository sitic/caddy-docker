ARG BASE_IMAGE=registry.gitlab.com/sitic/caddy-docker/base:latest
FROM $BASE_IMAGE

RUN apk add --no-cache \
            php7-simplexml \
            php7-xmlwriter \
            php7-pdo_sqlite \
            php7-cgi \
            php7-zlib \
            libcap \
            rrdtool

RUN setcap 'cap_net_bind_service=+ep' /usr/bin/caddy

RUN addgroup -g 1005 -S abc && \
    adduser -u 1005 -S abc -G abc
    
RUN sed -i 's/^;error_log =/error_log = \/proc\/self\/fd\/2\n;error_log =/g' /etc/php7/php-fpm.conf && \
    sed -i 's/^;access_log =/access_log = \/proc\/self\/fd\/2\n;access_log =/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/^;catch_workers_output = /catch_workers_output = /g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/^user = .*/user = abc/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/^group = .*/group = abc/g' /etc/php7/php-fpm.d/www.conf
    
USER 1005
ENTRYPOINT ["/bin/parent", "caddy"]
CMD ["--conf", "/srv/Caddyfile", "--log", "stdout", "--agree=true", "-port", "216"]